package parser;

import dto.Book;
import dto.Root;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Worker {

    private static final String TAG_NAME = "name";
    private static final String TAG_ID = "id";
    private static final String TAG_EDITION = "edition";
    private static final String TAG_LANGUAGE = "language";
    private static final String TAG_AUTHOR = "author";


    public Root work() {

        Root root = new Root();

        JSONParser parser = new JSONParser();

        try (FileReader reader = new FileReader("test.json")) {
            JSONObject parse = (JSONObject) parser.parse(reader);

            String name = (String) parse.get(TAG_NAME);
            root.setName(name);

            JSONArray jsonArray = (JSONArray) parse.get("book");
            ArrayList<Book> books = new ArrayList<>();
            for (Object it : jsonArray) {
                JSONObject JsonBook = (JSONObject) it;

                long bookId = (long) JsonBook.get(TAG_ID);
                String bookEdition = (String) JsonBook.get(TAG_EDITION);
                String bookLanguage = (String) JsonBook.get(TAG_LANGUAGE);
                String bookAuthor = (String) JsonBook.get(TAG_AUTHOR);

                Book inputBook = new Book((int) bookId, bookLanguage, bookEdition, bookAuthor);
                books.add(inputBook);
            }
            root.setBooks(books);

            return root;

        } catch (ParseException e) {
            e.printStackTrace();
            return root;
        } catch (IOException e) {
            e.printStackTrace();
            return root;
        }
    }
}

